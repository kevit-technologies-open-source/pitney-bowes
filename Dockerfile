FROM node:10-alpine AS base
RUN apk add --update \
    python \
    python-dev \
    py-pip \
    build-base \
    mongodb-tools
COPY . .
RUN npm install

FROM base AS image
EXPOSE 5555
ENTRYPOINT ["node", "index.js"]
CMD [""]