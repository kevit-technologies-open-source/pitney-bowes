/**
 * System and 3rd Party libs
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Schema Definition
 */

const userSchema = new Schema({
    email: {type:Schema.Types.String,index:{unique:true},unique:true},
    mobile: Schema.Types.String,
    currier:[{
        toAddress:Schema.Types.Object,
        fromAddress:Schema.Types.Object,
        GTS:Schema.Types.Number,
        postalService:{type:Schema.Types.String,enum:['Newgistics','USPS','UPS']},
        printAddress:Schema.Types.Boolean,
        sendPackageTime:Schema.Types.Date,
        sendPackageDate:Schema.Types.Date
    }],
});

/**
 * Export Schema
 */
module.exports = mongoose.model('user', userSchema);
