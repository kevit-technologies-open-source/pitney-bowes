const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dialogflowTokenSchema = new Schema({
    accessToken: {type: Schema.Types.String, required: true, unique: true},
    createdAt: {type: Schema.Types.Date, default: Date.now()},
});
dialogflowTokenSchema.index({createdAt: 1}, {expireAfterSeconds: 3400});
module.exports = mongoose.model('dialogflow-token', dialogflowTokenSchema);
