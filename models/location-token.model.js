const mongoose = require('mongoose')
const Schema = mongoose.Schema

const locationTokenSchema = new Schema({
    accessToken: {type: Schema.Types.String, required: true, unique: true},
    createdAt: {type: Schema.Types.Date, default: Date.now()},
});
locationTokenSchema.index({createdAt: 1}, {expireAfterSeconds: 35900});

module.exports = mongoose.model('location-token', locationTokenSchema);
