# Pitney Bowes

## 1. Overview

This repo holds the source for the pitney bowes bot. The bot features NLP capabilities & integration with various APIs from Pitney Bowes & Algorithmia. Bot is mainly built in Microsoft Bot Framework and in NodeJS. 

## 2. Architecture

![Pitney bowes - architecture](./imgs/Pitney bowes - architecture.png)

### 2.1 User interface

- User facing components through which user will be interacting. 
- Currently, it's a web based interface, but can be integrated on any platform and any size of devices. 

### 2.2 Azure bot channel registration

- Azure bot channel registration is used to route the messages from user interface to our bot logic. 
- This component can be replaced with custom routing solution.

### 2.3 Bot Components

- Bot components are the brains of the bot. They handle the bot logic. 
- Bot logic uses database to store session. By storing sessions in database, bot service is stateless and more scalable. 

### 2.4 Service integrations

- Bot logic uses service integrations to communicate with 3rd party services. 
- In current POC, bot uses following service integrations:
  - Pitney Bowes API: Shipping APIs, Location APIs. 
  - Algorithmia: Sentiment APIs.
  - DialogFlow: NLP APIs to detect user intents. 

## 3. Configuration

### 3.1 Prerequisite

Some prior knowledge to following technologies:

- NodeJS
- Microsoft Bot Framework
- MongoDB
- Docker & Docker compose
- Google account
- Azure account
- Pitney Bowes account
- Algorithmia account

Setting up the stack would need following software requirements:

- Docker & Docker compose

### 3.2 Import DialogFlow agent

- Check [this guide](https://miningbusinessdata.com/how-to-import-an-agent-zip-file-into-api-ai/) to know how you can import the DialogFlow agent.
- The zip file to restore agent is in the source code with name `Hackathon-PitneyBowes.zip` 

### 3.3 Get service account credentials for Dialogflow

- To get the service account credentials, go to "Settings" > "General Settings" > click on "Service Account" name. ![Screenshot 2019-09-28 at 6.38.20 PM](./imgs/Screenshot 2019-09-28 at 6.38.20 PM.png)

- It will open Google Cloud Platform screen. Click on the 3 dot menu on right, and click on "Create key"![Screenshot 2019-09-28 at 6.41.26 PM](./imgs/Screenshot 2019-09-28 at 6.41.26 PM.png)

- In the popup, click on JSON type, and click on "Create". It will download a JSON file.![Screenshot 2019-09-28 at 6.44.16 PM](./imgs/Screenshot 2019-09-28 at 6.44.16 PM.png)

- Now, rename the JSOn file to `dialogflow-key.json` and put it inside `services/` folder in project directory. 

### 3.4 Bot channel registration

- Check the guide on [Azure](https://docs.microsoft.com/en-us/azure/bot-service/bot-service-quickstart-registration?view=azure-bot-service-3.0) on how to create bot channel registration. 

- Please note that you would need **Microsoft App id** & **Microsoft App password**. 

- Put Microsoft App id in the file `environment.env` in: `MICROSOFT_APP_ID`

- Put Microsoft App password in the file `environment.env` in: `MICROSOFT_APP_PASSWORD`

  ![Screenshot 2019-09-28 at 7.04.05 PM](./imgs/Screenshot 2019-09-28 at 7.04.05 PM.png)

### 3.5 Fetch Directline secret

- Please follow [this guide](https://docs.microsoft.com/en-us/azure/bot-service/bot-service-channel-connect-directline?view=azure-bot-service-4.0) to fetch directline secret.
- Once you have the secret, please put that secret inside `environment.env` file in: `DIRECTLINE_SECRET`![Screenshot 2019-09-28 at 7.09.09 PM](./imgs/Screenshot 2019-09-28 at 7.09.09 PM.png)

### 3.6 Get Google maps API key (used to display maps in frontend)

- Follow [this guide](https://developers.google.com/maps/documentation/javascript/get-api-key) to get the Google maps API key. 
- We need to enable 2 APIs in Google maps: 
  - Maps JavaScript API
  - Geocoding API
- Once you have the API key, put it into `environment.env` in: `GOOGLE_MAPS_API_KEY`![Screenshot 2019-09-28 at 7.20.38 PM](./imgs/Screenshot 2019-09-28 at 7.20.38 PM.png)

### 3.7 Create algorathmia key

- Log into your [algorithmia](https://algorithmia.com/) console, and copy your API key. 

![Screenshot 2019-09-28 at 7.14.48 PM](./imgs/Screenshot 2019-09-28 at 7.14.48 PM.png)

- Paste that API key into `environment.env` in: `ALGORITHM_KEY` ![Screenshot 2019-09-28 at 7.22.03 PM](./imgs/Screenshot 2019-09-28 at 7.22.03 PM.png)

### 3.8 Pitney Bowes keys

- Put pitney bowes API key in `environment.env` in : `PB_API_KEY`
- Put Pitney Bowes API key in `environment.env` in :`PB_API_SECRET` ![Screenshot 2019-09-28 at 7.25.09 PM](./imgs/Screenshot 2019-09-28 at 7.25.09 PM.png)

## 4. Get up and running

- First, let's build the image first, run following command:

  ```bash
  $ docker-compose build
  ```

- Then, bring up the services:

  ```bash
  $ docker-compose up
  ```

- It will bring up following services: 

  - Node app: Our bot logic
  - MongoDB: Database
  - Ngrok: service which lets you tunnel localhost traffics to internet. 

- We need to fetch the nginx URLs, for that go to http://localhost:4040

- Copy the URL with HTTPS, it should look something like https://bb5938c3.ngrok.io

- Now, go to our Bot channel registration that we created, and go to Settings.

- Insert the URL in Messaging endpoint. Now append /bot/messages in the URL. 

  - e.g. our URL is https://bb5938c3.ngrok.io so, before pasting in the box, it will become https://bb5938c3.ngrok.io/bot/messages

  ![Screenshot 2019-09-28 at 7.36.38 PM](./imgs/Screenshot 2019-09-28 at 7.36.38 PM.png)

- Now, navigate to http://localhost:3000 it will load the frontend and will start the bot. 