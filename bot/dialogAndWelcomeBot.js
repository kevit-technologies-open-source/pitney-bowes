const {DialogBot} = require('./dialogBot');

class DialogAndWelcomeBot extends DialogBot {
    constructor(conversationState, userState, dialog, userInfoPropertyAccessor) {
        super(conversationState, userState, dialog, userInfoPropertyAccessor);

        this.onConversationUpdate(async (turnContext, next) => {
            console.log(`Inside Conversation update`);
            // By calling next() you ensure that the next BotHandler is run.
            console.log(`From ID:`, turnContext.activity.from.id);

            this.userInfoPropertyAccessor = userInfoPropertyAccessor;
            await next();
        });

        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;
            for (let cnt = 0; cnt < membersAdded.length; cnt++) {
                if (membersAdded[cnt].id !== context.activity.recipient.id) {
                    await dialog.run(context, conversationState.createProperty('DialogState'));
                }
            }

            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });
    }
}

module.exports.DialogAndWelcomeBot = DialogAndWelcomeBot;
