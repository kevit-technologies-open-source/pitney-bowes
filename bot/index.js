/**
 * Router Configuration Files
 */

/**
 * System and 3rd party libs
 */
const express = require('express');
const router = express.Router();

// Import required bot services. See https://aka.ms/bot-services to learn more about the different parts of a bot.
const {BotFrameworkAdapter, ConversationState, InputHints, InspectionMiddleware, InspectionState, MemoryStorage, UserState} = require('botbuilder');


// This bot's main dialog.
const {DialogAndWelcomeBot} = require('./dialogAndWelcomeBot');

const {MicrosoftAppCredentials} = require('botframework-connector');

//User and conversation accessor require
const UserStateProperties = require('./../shared/UserStateProperties');
const ConversationStateProperties = require('./../shared/ConversationStateProperties');
const DialogsNameList = require('./../shared/DialogsNameList');

// Create adapter.
// See https://aka.ms/about-bot-adapter to learn more about adapters.
const adapter = new BotFrameworkAdapter({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});

// Catch-all for errors.
adapter.onTurnError = async (context, error) => {
    console.error(error);
    console.error(`\n [onTurnError]: ${error}`);
    // Send a message to the user
    const onTurnErrorMessage = `Sorry, it looks like something went wrong!`;
    await context.sendActivity(onTurnErrorMessage, onTurnErrorMessage, InputHints.ExpectingInput);
    // Clear out state
    await conversationState.delete(context);
};

// Define a state store for your bot. See https://aka.ms/about-bot-state to learn more about using MemoryStorage.
// A bot requires a state store to persist the dialog and user state between messages.

// For local development, in-memory storage is used.
// CAUTION: The Memory Storage used here is for local bot debugging only. When the bot

// is restarted, anything stored in memory will be gone.
const memoryStorage = new MemoryStorage();
const inspectionState = new InspectionState(memoryStorage);
const conversationState = new ConversationState(memoryStorage);
const userState = new UserState(memoryStorage);

adapter.use(new InspectionMiddleware(inspectionState, userState, conversationState, new MicrosoftAppCredentials(process.env.MICROSOFT_APP_ID, process.env.MICROSOFT_APP_PASSWORD)));

// Create user accessor property
let userInfoPropertyAccessor = userState.createProperty(UserStateProperties.USER_INFO_PROPERTY);


const {WelcomeDialog} = require('./dialogs/welcomeDialog');

const welcomeDialog = new WelcomeDialog(DialogsNameList.WELCOME_DIALOG, userInfoPropertyAccessor);


// Create and add the InspectionMiddleware to the adapter.
const bot = new DialogAndWelcomeBot(conversationState, userState, welcomeDialog, userInfoPropertyAccessor);

/**
 * Router Definitions
 */
router.get('/messages', function (req, res) {
    res.send('Hy');
});

router.post('/messages', (req, res) => {
    adapter.processActivity(req, res, async (turnContext) => {
        // bot.ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        // console.log('ip-------->',bot.ip);
        await bot.run(turnContext);
    });
});

/**
 * Export Router
 */
module.exports = router;
