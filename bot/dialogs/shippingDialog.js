const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const SHIPPING_WATERFALL_DIALOG = 'shippingWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')


class ShippingDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);
        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(SHIPPING_WATERFALL_DIALOG, [
                this.addressChoiceStep.bind(this),
                this.defaultAddressStep.bind(this),
                this.addressConfirmStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = SHIPPING_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async addressChoiceStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.SHIPPING_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: 'Sure, do you want to send package from your current address or enter a new one?',
            retryPrompt: 'Sure, do you want to send package from your current address or enter a new one?',
            choices: ChoiceFactory.toChoices(['Current Address', 'New Address']),
            //style: ListStyle.heroCard
        });
    }

    async defaultAddressStep(stepContext) {
        if (stepContext.result && stepContext.result.index === 0) {
            // await stepContext.context.sendActivity({type:ActivityTypes.Event,value:{eventName:'currentLocation'}});
            // return await stepContext.prompt(PromptsNameList.TEXT_PROMPT,{prompt:''});
            return await stepContext.next();
        } else if (stepContext.result && stepContext.result.index === 1) {
            return await stepContext.replaceDialog(DialogsNameList.ENTER_CURRENT_ADDRESS_DIALOG);
        }
    }

    async addressConfirmStep(stepContext) {
        // if(stepContext.result){
        let coordinates = JSON.parse("{\"latitude\":-105.240976,\"longitude\":40.018301}");
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        let address = await apiHelper.getAddressByLanAndLag(coordinates.latitude, coordinates.longitude);
        userData.data.fromAddress = address;
        userData.data.fromAddress.coordinates = [coordinates.latitude, coordinates.longitude];
        let addressStr = address.candidates[0].formattedStreetAddress + address.candidates[0].formattedLocationAddress;
        console.log('ADDRESS', addressStr)
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: {
                text: 'Are you sure you want to send it from ' + addressStr + '?'
                // speak: 'Are you sure you want to send it from <say-as interpret-as="address">' + addressStr + '</say-as>?'
            },
            choices: ChoiceFactory.toChoices(['Sure', 'No, Change Address']),
            //style: ListStyle.heroCard
        })
        // }
    }

    async nextStep(stepContext) {
        if (stepContext.result && stepContext.result.index === 0) {
            return await stepContext.replaceDialog(DialogsNameList.PARCEL_DIALOG);
        } else if (stepContext.result && stepContext.result.index === 1) {
            return await stepContext.replaceDialog(DialogsNameList.CHANGE_ADDRESS_DIALOG);
        }
    }
}


module.exports.ShippingDialog = ShippingDialog;
