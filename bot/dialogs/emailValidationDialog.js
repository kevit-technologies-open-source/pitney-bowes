const {MessageFactory, ActivityTypes, CardFactory, InputHints} = require('botbuilder');
const {DialogSet, DialogTurnStatus, TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const EMAIL_VALIDATION_WATERFALL_DIALOG = 'emailValidationWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
let User = require('../../models/user.model');
const logger = require('../../services/logger');

class EmailValidationDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new WaterfallDialog(EMAIL_VALIDATION_WATERFALL_DIALOG, [
            this.welcomeStep.bind(this),
            this.nextStep.bind(this)
        ]));

        this.initialDialogId = EMAIL_VALIDATION_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor;
    }

    async welcomeStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.EMAIL_VALIDATION_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        await stepContext.context.sendActivity({type: ActivityTypes.Event, value: {eventName: 'currentLocation'}})
        let message = 'Welcome to Pitney Bowes, I am your personal virtual assistant! Before we begin could you please enter your email id'
        if (stepContext.options && stepContext.options.retryPrompt)
            message = 'Please enter valid Email id';
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: message});
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        console.log('userData-->', userData);
        if (stepContext.result && userData.intent === IntentNameList.GET_EMAIL_INTENT && userData.parameters.email) {
            userData.data.email = userData.parameters.email;
            User.findOne({email: userData.data.email}).then(user => {
                if (!user)
                    new User({email: userData.data.email}).save().catch(e => logger.log.error('ERROR :', e.message));
            }).catch(e => logger.log.error('ERROR :', e.message));

            return await stepContext.replaceDialog(DialogsNameList.HELP_DIALOG);
        } else return await stepContext.replaceDialog(DialogsNameList.EMAIL_VALIDATION_DIALOG, {retryPrompt: true});
    }
}

module.exports.EmailValidationDialog = EmailValidationDialog;
