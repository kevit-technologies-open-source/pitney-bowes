const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const VIEW_MORE_WATERFALL_DIALOG = 'viewMoreWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')


class ViewMoreDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);
        this.addDialog(new WaterfallDialog(VIEW_MORE_WATERFALL_DIALOG, [
            this.optionsStep.bind(this),
            this.nextStep.bind(this)
        ]));
        this.initialDialogId = VIEW_MORE_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async optionsStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.VIEW_MORE_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: 'Oh sure, here are some other options for you',
            choices: ChoiceFactory.toChoices(['Manage Pending orders', 'Contact customer care']),
            style: ListStyle.heroCard
        });
    }

    async nextStep(stepContext) {
        if (stepContext.result && stepContext.result.index === 0) {
            return await stepContext.replaceDialog(DialogsNameList.MANAGE_PENDING_ORDERS_DIALOG);
        } else if (stepContext.result && stepContext.result.index === 1) {
            return await stepContext.replaceDialog(DialogsNameList.CUSTOMER_CARE_DIALOG);
        }
    }
}

module.exports.ViewMoreDialog = ViewMoreDialog;
