const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const POSTAL_SERVICE_WATERFALL_DIALOG = 'postalServicesWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')

class PostalServiceOptionsDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new WaterfallDialog(POSTAL_SERVICE_WATERFALL_DIALOG, [
            this.optionStep.bind(this),
            this.schedulePickUpStep.bind(this),
            this.nextStep.bind(this)
        ]));
        this.initialDialogId = POSTAL_SERVICE_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async optionStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        userData.prevDialog = DialogsNameList.POSTAL_SERVICE_OPTIONS_DIALOG
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: 'Here are some options for you?',
            choices: ChoiceFactory.toChoices(['Newgistics', 'USPS', 'UPS']),
        });
    }

    async schedulePickUpStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && stepContext.result.index === 0)
            userData.data.postalService = 'Newgistics'
        else if (stepContext.result && stepContext.result.index === 1)
            userData.data.postalService = 'USPS'
        else if (stepContext.result && stepContext.result.index === 2)
            userData.data.postalService = 'UPS'

        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: 'Alright, now that it\'s confirmed should I schedule a pickup for you?',
            choices: ChoiceFactory.toChoices(['confirm', 'cancel']),
            //style: ListStyle.heroCard
        });
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if ((stepContext.result && stepContext.result.index === 0) || userData.intent === IntentNameList.POSITIVE_INTENT)
            return await stepContext.replaceDialog(DialogsNameList.SCHEDULE_PICKUP_DIALOG);

        else if ((stepContext.result && stepContext.result.index === 0) || userData.intent === IntentNameList.NEGATIVE_INTENT)
            return await stepContext.replaceDialog(DialogsNameList.ANY_HELP_DIALOG);

    }
}

module.exports.PostalServiceOptionsDialog = PostalServiceOptionsDialog;
