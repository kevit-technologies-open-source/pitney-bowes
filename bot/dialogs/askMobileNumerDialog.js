const {MessageFactory, ActivityTypes, CardFactory, InputHints} = require('botbuilder');
const {DialogSet, DialogTurnStatus, TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const ASK_MOBILE_WATERFALL_DIALOG = 'askMobileWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');

class AskMobileNumberDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new WaterfallDialog(ASK_MOBILE_WATERFALL_DIALOG, [
            this.welcomeStep.bind(this),
            this.nextStep.bind(this)
        ]));

        this.initialDialogId = ASK_MOBILE_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor;
    }

    async welcomeStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.ASK_MOBILE_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Of course, please let me know the number we can call you at'});
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        if (stepContext.result && userData.parameters.mobileNumber) {
            userData.data.mobileNumber = userData.parameters.mobileNumber;
            await stepContext.context.sendActivity('Thank you so much! We will be contacting you soon.')
            return await stepContext.replaceDialog(DialogsNameList.ANY_HELP_DIALOG);
        } else return await stepContext.replaceDialog(DialogsNameList.ASK_EMAIL_DIALOG, {retryPrompt: true});
    }
}

module.exports.AskMobileNumberDialog = AskMobileNumberDialog;
