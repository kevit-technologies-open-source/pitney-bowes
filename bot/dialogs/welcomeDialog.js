const {MessageFactory, ActivityTypes, CardFactory, InputHints} = require('botbuilder');
const {DialogSet, DialogTurnStatus, TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const WELCOME_WATERFALL_DIALOG = 'welcomeWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');


//Import Dialogs
const {ShippingDialog} = require('./shippingDialog');
const {HelpDialog} = require('./helpDialog');
const {ParcelDialog} = require('./parcelDialog');
const {EnterCurrentAddressDialog} = require('./enterCurrentAddressDialog');
const {TrackingDialog} = require('./trackingDialog');
const {AnyHelpDialog} = require('./anyHelpDialog');
const {CheckAddressDialog} = require('./checkAddressDialog');
const {FeedbackDialog} = require('./feedbackDialog');
const {PostalServiceOptionsDialog} = require('./postalServiceOptionsDialog');
const {PrintDialog} = require('./printDialog');
const {RouteDialog} = require('./routeDialog');
const {SchedulePickUpDialog} = require('./schedulePickUpDialog');
const {CustomerCareDialog} = require('./customerCareDialog');
const {ManagePendingDialog} = require('./managePendingOrderDialog');
const {ViewMoreDialog} = require('./viewMoreDialog');
const {EmailValidationDialog} = require('./emailValidationDialog');
const {ChangeAddressDialog} = require('./changeAddressDialog');
const {TimeSchedulePickUpDialog} = require('./timeSchedulePickUpDialog');
const {DateSchedulePickUpDialog} = require('./dateSchedulePickUpDialog');
const {AskEmailDialog} = require('./askEmailDialog');
const {AskMobileNumberDialog} = require('./askMobileNumerDialog');
const {ShareAddressDialog} = require('./shareAddressDialog');
const {EnterAddressDialog} = require('./enterAddressDialog');

class WelcomeDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        //register all Dialogs
        this.addDialog(new ShippingDialog(DialogsNameList.SHIPPING_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new HelpDialog(DialogsNameList.HELP_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new ParcelDialog(DialogsNameList.PARCEL_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new EnterCurrentAddressDialog(DialogsNameList.ENTER_CURRENT_ADDRESS_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new TrackingDialog(DialogsNameList.TRACKING_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new AnyHelpDialog(DialogsNameList.ANY_HELP_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new CheckAddressDialog(DialogsNameList.CHECK_ADDRESS_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new FeedbackDialog(DialogsNameList.FEEDBACK_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new PostalServiceOptionsDialog(DialogsNameList.POSTAL_SERVICE_OPTIONS_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new PrintDialog(DialogsNameList.PRINT_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new RouteDialog(DialogsNameList.ROUTE_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new SchedulePickUpDialog(DialogsNameList.SCHEDULE_PICKUP_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new CustomerCareDialog(DialogsNameList.CUSTOMER_CARE_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new ManagePendingDialog(DialogsNameList.MANAGE_PENDING_ORDERS_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new ViewMoreDialog(DialogsNameList.VIEW_MORE_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new EmailValidationDialog(DialogsNameList.EMAIL_VALIDATION_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new ChangeAddressDialog(DialogsNameList.CHANGE_ADDRESS_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new TimeSchedulePickUpDialog(DialogsNameList.TIME_SCHEDULE_PICKUP_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new DateSchedulePickUpDialog(DialogsNameList.DATE_SCHEDULE_PICKUP_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new AskEmailDialog(DialogsNameList.ASK_EMAIL_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new AskMobileNumberDialog(DialogsNameList.ASK_MOBILE_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new ShareAddressDialog(DialogsNameList.SHARE_ADDRESS_DIALOG, userInfoPropertyAccessor));
        this.addDialog(new EnterAddressDialog(DialogsNameList.ENTER_ADDRESS_DIALOG, userInfoPropertyAccessor));

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT));
        this.addDialog(new ChoicePrompt(PromptsNameList.CHOICE_PROMPT))
            .addDialog(new WaterfallDialog(WELCOME_WATERFALL_DIALOG, [
                this.introStep.bind(this),
                this.nextStep.bind(this)
            ]));

        this.initialDialogId = WELCOME_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor;
    }

    /**
     * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} turnContext
     * @param {*} accessor
     */

    async run(turnContext, accessor) {
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);

        console.log("turnContext");
        console.log(turnContext.activity.type === ActivityTypes.ConversationUpdate);

        const dialogContext = await dialogSet.createContext(turnContext);

        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id);
        }
    }

    async introStep(stepContext) {
        await this.userInfoPropertyAccessor.set(stepContext.context, {
            prevDialog: DialogsNameList.WELCOME_DIALOG,
            parameters: {},
            data: {}
        });
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: ''});
    }

    async nextStep(stepContext) {
        let message = 'Welcome to Pitney Bowes, I am your personal virtual assistant';
        await stepContext.context.sendActivity(message);
        return await stepContext.replaceDialog(DialogsNameList.HELP_DIALOG)
    }

}


module.exports.WelcomeDialog = WelcomeDialog;
