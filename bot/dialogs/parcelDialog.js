const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const PARCEL_WATERFALL_DIALOG = 'parcelWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')

class ParcelDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(PARCEL_WATERFALL_DIALOG, [
                this.sendPackageStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = PARCEL_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async sendPackageStep(stepContext) {
        if (stepContext.options && stepContext.options.retryPrompt)
            return await stepContext.next();

        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.PARCEL_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        await stepContext.context.sendActivity('Okay, now can you please also tell me where you want to send the package?');
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: 'Would you like to share the location or enter it?',
            choices: ChoiceFactory.toChoices(['Share Location', 'Address']),
            //style: ListStyle.heroCard
        });
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        if ((stepContext.result && stepContext.result.index === 1) || userData.isEnterAddress) {
            userData.isEnterAddress = true;
            return await stepContext.replaceDialog(DialogsNameList.ENTER_ADDRESS_DIALOG);
        } else if ((stepContext.result && stepContext.result.index === 0) || userData.isShareAddress) {
            userData.isShareAddress = true;
            return await stepContext.replaceDialog(DialogsNameList.SHARE_ADDRESS_DIALOG);
        }
    }

}


module.exports.ParcelDialog = ParcelDialog;
