const {MessageFactory, ActivityTypes, CardFactory, InputHints} = require('botbuilder');
const {DialogSet, DialogTurnStatus, TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const ASK_EMAIL_WATERFALL_DIALOG = 'askEmailWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');

class AskEmailDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new WaterfallDialog(ASK_EMAIL_WATERFALL_DIALOG, [
            this.welcomeStep.bind(this),
            this.nextStep.bind(this)
        ]));

        this.initialDialogId = ASK_EMAIL_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor;
    }

    async welcomeStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.ASK_EMAIL_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Sure! Just let me know your email id you would like us to email at'});
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        if (stepContext.result && userData.intent === IntentNameList.GET_EMAIL_INTENT && userData.parameters.email) {
            userData.data.email = userData.parameters.email;
            await stepContext.context.sendActivity('Thank you so much! We will be getting back to you soon.')
            return await stepContext.replaceDialog(DialogsNameList.ANY_HELP_DIALOG);
        } else return await stepContext.replaceDialog(DialogsNameList.ASK_EMAIL_DIALOG, {retryPrompt: true});
    }
}

module.exports.AskEmailDialog = AskEmailDialog;
