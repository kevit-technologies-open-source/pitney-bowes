const {MessageFactory, ActivityTypes, InputHints, CardFactory} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const CHECK_ADDRESS_WATERFALL_DIALOG = 'checkAddressWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')

class CheckAddressDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(CHECK_ADDRESS_WATERFALL_DIALOG, [
                this.checkGeoRiskStep.bind(this),
                this.giveAddressStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = CHECK_ADDRESS_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async checkGeoRiskStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        await stepContext.context.sendActivity('Perfect, now that we have both the addresses, let me retrieve information about the location you want to send package to.');
        console.log('userData------------------------>', userData.data)
        if (userData.data.toAddress.coordinates) {
            let coordinates = userData.data.toAddress.coordinates;
            try {
                let result = await Promise.all([apiHelper.riskEarthquake(coordinates[0], coordinates[1]), apiHelper.riskFlood(coordinates[0], coordinates[1]), apiHelper.riskFire(coordinates[0], coordinates[1])]);
                if (result[0] || result[1] || result[2]) {
                    await stepContext.context.sendActivity('Here is risk result');
                    await stepContext.context.sendActivity({attachments: [this.createReceiptCard(result)]});
                }
            } catch (e) {
                console.log('ERROR:', e.message)
            }
        }
        let isRiskyAddress = false;
        if (isRiskyAddress)
            return await stepContext.next();
        else
            return await stepContext.replaceDialog(DialogsNameList.ROUTE_DIALOG);
    }

    async giveAddressStep(stepContext) {
        await stepContext.context.sendActivity('Oh no, looks like there is an earthquake in that area, you package delivery may be delayed.');
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Are you sure you want to send the package?'})
    }

    async nextStep(stepContext) {
        let userData = await stepContext.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && userData.intent === IntentNameList.POSITIVE_INTENT)
            return await stepContext.replaceDialog(DialogsNameList.ROUTE_DIALOG);
        else if (stepContext.result && userData.intent === IntentNameList.NEGATIVE_INTENT)
            return stepContext.replaceDialog(DialogsNameList.HELP_DIALOG);
    }

    createReceiptCard(result) {
        let earthRisk = result[0] ? {key: 'Earthquake', value: result[0]} : {};
        let floodRisk = result[1] ? {key: 'Flood', value: result[1]} : {};
        let fireRisk = result[2] ? {key: 'Fire', value: result[2]} : {};
        return CardFactory.receiptCard({
            title: 'Risk result',
            facts: [
                earthRisk,
                floodRisk,
                fireRisk
            ]
        });
    }
}


module.exports.CheckAddressDialog = CheckAddressDialog;
