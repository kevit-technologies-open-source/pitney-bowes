const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const HELP_WATERFALL_DIALOG = 'helpWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');


class HelpDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);
        this.addDialog(new ChoicePrompt(PromptsNameList.CHOICE_PROMPT))
            .addDialog(new WaterfallDialog(HELP_WATERFALL_DIALOG, [
                this.helpStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = HELP_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }


    async helpStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.HELP_DIALOG
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: 'What can I help you with today?',
            choices: ChoiceFactory.toChoices(['Shipping', 'Tracking', 'View More']),
            //style: ListStyle.heroCard
        });
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        console.log('stepContext.context.activity.from', stepContext.context.activity.from);
        if ((stepContext.result && stepContext.result.index === 0) || userData.intent === IntentNameList.SHIPPING_INTENT) {
            return await stepContext.replaceDialog(DialogsNameList.SHIPPING_DIALOG);
        } else if ((stepContext.result && stepContext.result.index === 1) || userData.intent === IntentNameList.TRACKING_INTENT) {
            return await stepContext.replaceDialog(DialogsNameList.TRACKING_DIALOG);
        } else if ((stepContext.result && stepContext.result.index === 2) || userData.intent === IntentNameList.VIEW_MORE_INTENT) {
            return await stepContext.replaceDialog(DialogsNameList.VIEW_MORE_DIALOG);
        }
        return await stepContext.replaceDialog(DialogsNameList.WELCOME_DIALOG);
    }
}


module.exports.HelpDialog = HelpDialog;
