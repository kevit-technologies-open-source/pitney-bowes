const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const CHANGE_ADDRESS_WATERFALL_DIALOG = 'changeAddressWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper');

class ChangeAddressDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(CHANGE_ADDRESS_WATERFALL_DIALOG, [
                this.promptChoiceStep.bind(this),
                this.giveAddressStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = CHANGE_ADDRESS_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async promptChoiceStep(stepContext) {
        if (stepContext.options && stepContext.options.retryPrompt)
            return await stepContext.next();

        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.CHANGE_ADDRESS_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: 'Would you like to share the location or enter it?',
            choices: ChoiceFactory.toChoices(['Share Location', 'Address']),
            //style: ListStyle.heroCard
        });
    }

    async giveAddressStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        if ((stepContext.result && stepContext.result.index === 1) || (userData.enterAddress || userData.isEnterAddress)) {
            userData.enterAddress = true;
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            await stepContext.context.sendActivity({type: ActivityTypes.Event, value: {eventName: 'shareLocation'}});
            return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Sure, Please tell me address'});
        } else if ((stepContext.result && stepContext.result.index === 0) || (userData.shareAddress || userData.isShareAddress)) {
            userData.shareAddress = true;
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Sure, please share your location'})
        }
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        if (stepContext.result && userData.intent === IntentNameList.GET_ADDRESS_INTENT && userData.parameters.location && userData.enterAddress) {
            userData.isToAddress ? userData.data.toAddress = userData.parameters.location : userData.data.fromAddress = userData.parameters.location;
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            let address = await apiHelper.getGeoLocation(userData.isToAddress ? (userData.data.isToAddress['street-address'] + ' ' + userData.data.isToAddress['subadmin-area']) : (userData.data.fromAddress['street-address'] + ' ' + userData.data.fromAddress['subadmin-area']))
            console.log('address======>', address);
            userData.isToAddress ? userData.data.toAddress.coordinates = address.coordinates : userData.data.fromAddress.coordinates = address.coordinates
            userData.isToAddress ? userData.toAddress.formattedAddress = address.formattedAddress : userData.fromAddress.formattedAddress = address.formattedAddress
            let status = await apiHelper.addressValidation({
                address: address.formattedAddress,
                zipCode: address.postCode
            })
            if (userData.isToAddress)
                return await stepContext.replaceDialog(DialogsNameList.CHECK_ADDRESS_DIALOG)

            return await stepContext.replaceDialog(DialogsNameList.PARCEL_DIALOG);
        } else if (stepContext.result && userData.shareAddress) {
            let coordinates = JSON.parse(stepContext.result);
            let address = await apiHelper.getAddressByLanAndLag(coordinates.latitude, coordinates.longitude);
            userData.isToAddress ? userData.data.toAddress = address : userData.data.fromAddress = address;
            userData.isToAddress ? userData.data.isToAddress.coordinates = [coordinates.latitude, coordinates.longitude] : userData.data.fromAddress.coordinates = [coordinates.latitude, coordinates.longitude];
            if (userData.isToAddress)
                return await stepContext.replaceDialog(DialogsNameList.CHECK_ADDRESS_DIALOG)

            return await stepContext.replaceDialog(DialogsNameList.PARCEL_DIALOG);
        } else {
            return await stepContext.replaceDialog(DialogsNameList.CHANGE_ADDRESS_DIALOG, {retryPrompt: true})
        }
    }
}


module.exports.ChangeAddressDialog = ChangeAddressDialog;
