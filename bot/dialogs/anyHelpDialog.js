const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const ANY_WATERFALL_DIALOG = 'anyWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');

class AnyHelpDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);
        this.addDialog(new ChoicePrompt(PromptsNameList.CHOICE_PROMPT))
            .addDialog(new WaterfallDialog(ANY_WATERFALL_DIALOG, [
                this.helpAskStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = ANY_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async helpAskStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        userData.prevDialog = DialogsNameList.ANY_HELP_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: 'Alright then, May I help you with anything else then today?',
            choices: ChoiceFactory.toChoices(['Yes', 'No thanks']),
            //style : ListStyle.heroCard
        });
    }

    async nextStep(stepContext) {
        if (stepContext.result && stepContext.result.index === 1) {
            console.log('INSIDE GO TO FEEDBACK DIALOG')
            return await stepContext.replaceDialog(DialogsNameList.FEEDBACK_DIALOG);
        } else if (stepContext.result && stepContext.result.index === 0)
            return await stepContext.replaceDialog(DialogsNameList.HELP_DIALOG);
    }
}


module.exports.AnyHelpDialog = AnyHelpDialog;
