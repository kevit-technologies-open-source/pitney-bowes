const {ComponentDialog} = require('botbuilder-dialogs');
const DFToken = require('../../models/dialogflow-token.model');
const googleAuth = require('google-oauth-jwt');
const Logger = require('../../services/logger');
const dialogFlowKeys = require('../../services/dialogflow-key');
const rp = require('request-promise');
const DialogsNameList = require('../../shared/DialogsNameList');
const IntentNameList = require('../../shared/IntentNameList');

/**
 * This base class watches for common phrases like "help" and "cancel" and takes action on them
 * BEFORE they reach the normal bot logic.
 */
class DialogFlowRecognizer extends ComponentDialog {
    async onContinueDialog(innerDc) {
        const result = await this.interrupt(innerDc);
        if (result) {
            return result;
        }
        return await super.onContinueDialog(innerDc);
    }

    async interrupt(innerDc) {

        if (innerDc.context.activity.text) {
            let text;
            text = innerDc.context.activity.text;
            try {
                // console.log('Running dialog with Message Activity.');
                let access_token = await createGoogleToken();
                // console.log("Token is Get");
                let options = {
                    method: 'POST',
                    url: `https://dialogflow.googleapis.com/v2/projects/${dialogFlowKeys.project_id}/agent/sessions/123:detectIntent?access_token=` + access_token,
                    body: {
                        "queryInput":
                            {
                                "text": {
                                    "text": text.toLowerCase(),
                                    "languageCode": "en"
                                }
                            }
                    },
                    auth: {
                        'bearer': access_token
                    },
                    json: true
                };

                let response = await rp(options);
                // console.log('options', JSON.stringify(response,null,2));
                let userData = await this.userInfoPropertyAccessor.get(innerDc.context);
                // console.log('userData---->', userData);
                if (response.queryResult.intent) {
                    let intent = response.queryResult.intent.displayName
                    userData.intent = intent;
                    userData.parameters = {}

                    //Add Parameters in UserData
                    if (response.queryResult.parameters) {
                        let parameter = response.queryResult.parameters;
                        if (parameter.email && (userData.prevDialog === DialogsNameList.EMAIL_VALIDATION_DIALOG || userData.prevDialog === DialogsNameList.ASK_EMAIL_DIALOG))
                            userData.parameters.email = parameter.email;
                        if (parameter.location)
                            userData.parameters.location = parameter.location;
                        if (parameter.date)
                            userData.parameters.date = parameter.date;
                        if (parameter.time)
                            userData.parameters.time = parameter.time;
                        if ((parameter.mobileNumber || parameter.id) && userData.prevDialog === DialogsNameList.TRACKING_DIALOG)
                            userData.parameters.trackingId = parameter.mobileNumber ? parseInt(parameter.mobileNumber) : parseInt(parameter.id)
                        if ((parameter.mobileNumber || parameter.id) && userData.prevDialog === DialogsNameList.ASK_MOBILE_DIALOG)
                            userData.parameters.mobileNumber = parameter.mobileNumber ? parameter.mobileNumber : parameter.id
                    }

                    if (intent === IntentNameList.SHIPPING_INTENT && userData.data.email) {
                        return await innerDc.replaceDialog(DialogsNameList.SHIPPING_DIALOG)
                    }
                    if (intent === IntentNameList.TRACKING_INTENT && userData.data.email && !userData.prevDialog === DialogsNameList.TRACKING_DIALOG) {
                        return await innerDc.replaceDialog(DialogsNameList.TRACKING_DIALOG)
                    }
                    if ((intent === IntentNameList.VIEW_MORE_INTENT || intent === IntentNameList.MORE_OPTIONS_INTENT) && userData.data.email && !(userData.prevDialog === DialogsNameList.ROUTE_DIALOG)) {
                        return await innerDc.replaceDialog(DialogsNameList.VIEW_MORE_DIALOG)
                    }

                    switch (userData.prevDialog) {
                        case DialogsNameList.EMAIL_VALIDATION_DIALOG:

                        //shipping Dialog
                        case DialogsNameList.SHIPPING_DIALOG :
                            if (intent === IntentNameList.POSITIVE_INTENT)
                                innerDc.context.activity.text = "sure";
                            if (intent === IntentNameList.NEGATIVE_INTENT)
                                innerDc.context.activity.text = "No, Change Address";
                            if (intent === IntentNameList.CURRENT_ADDRESS_INTENT)
                                innerDc.context.activity.text = "Current Address";
                            if (intent === IntentNameList.ENTER_ADDRESS_INTENT)
                                innerDc.context.activity.text = "New Address";
                            break;

                        //parcel Dialog
                        case DialogsNameList.PARCEL_DIALOG :
                            if (intent === IntentNameList.SHARE_LOCATION_INTENT)
                                innerDc.context.activity.text = 'Share Location';
                            if (intent === IntentNameList.ENTER_ADDRESS_INTENT)
                                innerDc.context.activity.text = 'Address';
                            if (intent === IntentNameList.POSITIVE_INTENT)
                                innerDc.context.activity.text = 'confirm';
                            if (intent === IntentNameList.NEGATIVE_INTENT)
                                innerDc.context.activity.text = 'cancel';
                            break;

                        //share Address Dialog
                        case DialogsNameList.SHARE_ADDRESS_DIALOG :
                            if (intent === IntentNameList.POSITIVE_INTENT)
                                innerDc.context.activity.text = 'confirm';
                            if (intent === IntentNameList.NEGATIVE_INTENT)
                                innerDc.context.activity.text = 'cancel';
                            break;

                        //enter Address Dialog
                        case DialogsNameList.ENTER_ADDRESS_DIALOG :
                            if (intent === IntentNameList.POSITIVE_INTENT)
                                innerDc.context.activity.text = 'confirm';
                            if (intent === IntentNameList.NEGATIVE_INTENT)
                                innerDc.context.activity.text = 'cancel';
                            break;

                        //enter Current Address Dialog
                        case DialogsNameList.ENTER_CURRENT_ADDRESS_DIALOG :
                            if (intent === IntentNameList.POSITIVE_INTENT)
                                innerDc.context.activity.text = 'confirm';
                            if (intent === IntentNameList.NEGATIVE_INTENT)
                                innerDc.context.activity.text = 'No';
                            break;

                        //postal service options dialog
                        case DialogsNameList.POSTAL_SERVICE_OPTIONS_DIALOG:
                            if (intent === IntentNameList.NEWGISTICS_INTENT)
                                innerDc.context.activity.text = 'Newgistics';
                            if (intent === IntentNameList.USPS_INTENT)
                                innerDc.context.activity.text = 'USPS';
                            if (intent === IntentNameList.UPS_INTENT)
                                innerDc.context.activity.text = 'UPS';
                            if (intent === IntentNameList.POSITIVE_INTENT)
                                innerDc.context.activity.text = 'confirm';
                            if (intent === IntentNameList.NEGATIVE_INTENT)
                                innerDc.context.activity.text = 'cancel';
                            break;

                        //print dialog
                        case DialogsNameList.PRINT_DIALOG:
                            if (intent === IntentNameList.PRINT_INTENT)
                                innerDc.context.activity.text = 'Print';
                            if (intent === IntentNameList.NEGATIVE_INTENT)
                                innerDc.context.activity.text = 'Cancel';
                            break;

                        //any help Dialog
                        case DialogsNameList.ANY_HELP_DIALOG:
                            if (intent === IntentNameList.POSITIVE_INTENT)
                                innerDc.context.activity.text = 'Yes';
                            if (intent === IntentNameList.NEGATIVE_INTENT)
                                innerDc.context.activity.text = 'No thanks';
                            break;

                        //view more Dialog
                        case DialogsNameList.VIEW_MORE_DIALOG:
                            if (intent === IntentNameList.MANAGE_PENDING_ORDERS_INTENT)
                                innerDc.context.activity.text = 'Manage Pending orders';
                            if (intent === IntentNameList.CONTACT_CUSTOMER_CARE_INTENT || intent === IntentNameList.CALL_CUSTOMER_CARE_INTENT)
                                innerDc.context.activity.text = 'Contact customer care';
                            if (intent === IntentNameList.VIEW_CARRIER_OPTIONS_INTENT)
                                innerDc.context.activity.text = 'View carrier analytics';
                            break;

                        //manage Pending Order Dialog
                        case DialogsNameList.MANAGE_PENDING_ORDERS_DIALOG :
                            if (intent === IntentNameList.SHIPPING_INTENT)
                                innerDc.context.activity.text = 'ship'
                            if (intent === IntentNameList.TRACKING_INTENT)
                                innerDc.context.activity.text = 'track'
                            if (intent === IntentNameList.NEGATIVE_INTENT)
                                innerDc.context.activity.text = 'cancel'

                            break;

                        //customer care Dialog
                        case DialogsNameList.CUSTOMER_CARE_DIALOG :
                            if (intent === IntentNameList.CALL_CUSTOMER_CARE_INTENT)
                                innerDc.context.activity.text = 'Call';
                            if (intent === IntentNameList.EMAIL_CUSTOMER_CARE_INTENT)
                                innerDc.context.activity.text = 'Email';
                            break;


                        //tracking Dialog
                        case DialogsNameList.TRACKING_DIALOG :
                            if (intent === IntentNameList.TRACKING_INTENT) {
                                innerDc.context.activity.text = 'Track Shipment'
                            }
                            if (intent === IntentNameList.CANCEL_SHIPMENT_INTENT) {
                                innerDc.context.activity.text = 'Cancel Shipment'
                            }
                            if (intent === IntentNameList.POSITIVE_INTENT) {
                                innerDc.context.activity.text = 'Confirm';
                            }
                            if (intent === IntentNameList.NEGATIVE_INTENT) {
                                innerDc.context.activity.text = 'Cancel';
                            }
                            break;
                    }
                    await this.userInfoPropertyAccessor.set(innerDc.context, userData)
                }

            } catch (e) {
                console.log("Error in the DialogFlow Recognizer", e)
            }
        }
    }
}

let createGoogleToken = () => {
    return new Promise((resolve, reject) => {
        DFToken.find({}).then(d => {
            if (d.length > 0) {
                // Logger.log.info("Token Getting ", d[0].accessToken);
                resolve(d[0].accessToken)
            } else {
                Logger.log.warn('Token is not In Db creating new Token');
                googleAuth.authenticate(
                    {
                        email: dialogFlowKeys.client_email,
                        key: dialogFlowKeys.private_key,
                        scopes: ['https://www.googleapis.com/auth/cloud-platform', 'https://www.googleapis.com/auth/dialogflow']
                    },
                    (err, token) => {
                        if (err) {
                            Logger.log.error("Error to Generating Token From google api call", err);
                            reject(err)
                        } else {
                            // console.log("New Token Generated", token);
                            new DFToken({accessToken: token}).save().then(d => {
                                console.log("New Token Updated", d.accessToken);
                                resolve(d.accessToken)
                            }).catch(e => {
                                Logger.log.error("Error in update Token", e);
                                reject(e)
                            })

                        }
                    },
                );
            }
        }).catch(e => {
            Logger.log.error("Database Error in Get Token", e);
            reject(e)
        })
    })
};

module.exports.DialogFlowRecognizer = DialogFlowRecognizer;
