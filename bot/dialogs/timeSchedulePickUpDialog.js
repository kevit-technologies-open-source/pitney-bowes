const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const TIME_SCHEDULE_PICKUP_WATERFALL_DIALOG = 'timeSchedulePickUpWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')

class TimeSchedulePickUpDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(TIME_SCHEDULE_PICKUP_WATERFALL_DIALOG, [
                this.timePromptStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = TIME_SCHEDULE_PICKUP_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async timePromptStep(stepContext) {
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Cool, and at what time?'});
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && userData.parameters.time) {
            userData.data.sendPackageTime = userData.parameters.time
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            return await stepContext.replaceDialog(DialogsNameList.PRINT_DIALOG);
        } else return await stepContext.replaceDialog(DialogsNameList.TIME_SCHEDULE_PICKUP_DIALOG);
    }
}


module.exports.TimeSchedulePickUpDialog = TimeSchedulePickUpDialog;
