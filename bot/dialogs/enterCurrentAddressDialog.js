const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const ENTER_CURRENT_ADDRESS_WATERFALL_DIALOG = 'enterCurrentAddressWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper');

class EnterCurrentAddressDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);
        this.addDialog(new WaterfallDialog(ENTER_CURRENT_ADDRESS_WATERFALL_DIALOG, [
            this.addressChoiceStep.bind(this),
            this.confirmAddressStep.bind(this),
            this.nextStep.bind(this)
        ]));
        this.initialDialogId = ENTER_CURRENT_ADDRESS_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor;
    }

    async addressChoiceStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        userData.prevDialog = DialogsNameList.ENTER_CURRENT_ADDRESS_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Of course, Please tell me the address you want to send this package from.'});
    }

    async confirmAddressStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && userData.intent === IntentNameList.GET_ADDRESS_INTENT && userData.parameters.location) {
            userData.data.fromAddress = userData.parameters.location;
            let address = await apiHelper.getGeoLocation(userData.data.fromAddress['street-address'] + ' ' + userData.data.fromAddress['subadmin-area']);
            console.log('address======>', address);
            userData.data.fromAddress.coordinates = address.coordinates;
            userData.data.fromAddress.formattedAddress = address.formattedAddress;
            let status = await apiHelper.addressValidation({
                address: address.formattedAddress,
                zipCode: address.postCode
            });
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
                prompt: {
                    text: 'Let me just confirm, you want to send your package from ' + address.formattedAddress + '?',
                    speak: 'Let me just confirm, you want to send your package from <say-as interpret-as="address">' + address.formattedAddress + '</say-as>?'
                },
                choices: ChoiceFactory.toChoices(['Confirm', 'No']),
                //style: ListStyle.heroCard
            });
        } else return await stepContext.replaceDialog(DialogsNameList.ENTER_CURRENT_ADDRESS_DIALOG);
    }

    async nextStep(stepContext) {
        if (stepContext.result && stepContext.result.index === 0) {
            return await stepContext.replaceDialog(DialogsNameList.PARCEL_DIALOG);
        } else {
            return await stepContext.replaceDialog(DialogsNameList.CHANGE_ADDRESS_DIALOG);
        }
    }
}

module.exports.EnterCurrentAddressDialog = EnterCurrentAddressDialog;
