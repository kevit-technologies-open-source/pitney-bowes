const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const SHARE_ADDRESS_WATERFALL_DIALOG = 'shareAddressWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')

class ShareAddressDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(SHARE_ADDRESS_WATERFALL_DIALOG, [
                this.giveAddressStep.bind(this),
                this.confirmAddressStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = SHARE_ADDRESS_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }


    async giveAddressStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.SHARE_ADDRESS_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        await stepContext.context.sendActivity({type: ActivityTypes.Event, value: {eventName: 'shareLocation'}});
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Sure, please share your location'})
    }

    async confirmAddressStep(stepContext) {
        if (stepContext.result) {
            console.log('stepContext.result', stepContext.result);
            let coordinates = JSON.parse(stepContext.result);
            let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
            let address = await apiHelper.getAddressByLanAndLag(coordinates.latitude, coordinates.longitude);
            userData.data.toAddress = address;
            userData.data.toAddress.coordinates = [coordinates.latitude, coordinates.longitude]
            let addressStr = address.candidates[0] ? (address.candidates[0].formattedStreetAddress ? address.candidates[0].formattedStreetAddress : 'Address not found') + (address.candidates[0].formattedLocationAddress ? address.candidates[0].formattedLocationAddress : '') : 'Address not Found'
            console.log('ADDRESS', addressStr)
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
                prompt: 'Are you sure you want to send the package to ' + addressStr,
                choices: ChoiceFactory.toChoices(['confirm', 'cancel']),
                //style: ListStyle.heroCard
            });
        } else return await stepContext.replaceDialog(DialogsNameList.PARCEL_DIALOG, {retryPrompt: true})
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && stepContext.result.index === 0) {
            return stepContext.replaceDialog(DialogsNameList.CHECK_ADDRESS_DIALOG);
        } else if (stepContext.result && stepContext.result.index === 1) {
            userData.isToAddress = true;
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            return stepContext.replaceDialog(DialogsNameList.CHANGE_ADDRESS_DIALOG);
        }
    }
}


module.exports.ShareAddressDialog = ShareAddressDialog;
