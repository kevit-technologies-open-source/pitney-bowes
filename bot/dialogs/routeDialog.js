const {MessageFactory, ActivityTypes, InputHints, CardFactory} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const ROUTE_WATERFALL_DIALOG = 'shippingWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')

class RouteDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(ROUTE_WATERFALL_DIALOG, [
                this.displayRouteStep.bind(this),
                this.gtsResultStep.bind(this),
                this.postalServiceStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = ROUTE_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async displayRouteStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        userData.prevDialog = DialogsNameList.ROUTE_DIALOG
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        console.log('Into the Dialog')
        console.log(JSON.stringify(userData, null, 2));
        let fromAddress = (userData.data.fromAddress.candidates && userData.data.fromAddress.candidates[0]) ? (userData.data.fromAddress.candidates[0].formattedStreetAddress + ' ' + userData.data.fromAddress.candidates[0].formattedLocationAddress) : userData.data.fromAddress.formattedAddress;
        let toAddress = (userData.data.toAddress.candidates && userData.data.toAddress.candidates[0]) ? (userData.data.toAddress.candidates[0].formattedStreetAddress + ' ' + userData.data.toAddress.candidates[0].formattedLocationAddress) : userData.data.toAddress.formattedAddress
        let route = await apiHelper.geoRoute(fromAddress, toAddress);

        //TODO: Add SSML code for better audio experience.
        if (fromAddress && toAddress)
            await stepContext.context.sendActivity({
                text: 'Okay your courier is deliver in ' + route.time + ' ' + route.timeUnit + ' and total distance ' + route.distance + ' ' + route.distanceUnit
                // speak: 'Okay your courier is deliver in  ' + route.time + ' ' + route.timeUnit + ' and total distance ' + route.distance + ' ' + route.distanceUnit
            });

        return await stepContext.next();
        // return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT,{prompt:'Would you like to share the location or enter it?',
        //     choices: ChoiceFactory.toChoices(['Share Location','Address']),
        //
        // });

    }

    async gtsResultStep(stepContext) {
        await stepContext.context.sendActivity('Here is an approximate duty fee you will be paying for your shipment');
        await stepContext.context.sendActivity({attachments: [this.createReceiptCard()]});
        return await stepContext.next();
    }

    async postalServiceStep(stepContext) {
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'I suggest you send your package with Newgistic Postal services. Should I confirm pickup or do you want to view more carrier options?'});
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && userData.intent === IntentNameList.MORE_OPTIONS_INTENT) {
            return await stepContext.replaceDialog(DialogsNameList.POSTAL_SERVICE_OPTIONS_DIALOG);
        } else if (stepContext.result && (userData.intent === IntentNameList.NEWGISTICS_INTENT || userData.intent === IntentNameList.POSITIVE_INTENT)) {
            userData.data.postalService = 'Newgistics';
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            return await stepContext.replaceDialog(DialogsNameList.SCHEDULE_PICKUP_DIALOG);
        } else {
            return await stepContext.replaceDialog(DialogsNameList.POSTAL_SERVICE_OPTIONS_DIALOG);
        }
    }

    createReceiptCard() {
        return CardFactory.receiptCard({
            speak: 'Total cost will be $5.40 and of which $0.50 will be tax amount.',
            title: 'Global Trade Solutions',
            facts: [
                {
                    key: 'Order Number',
                    value: '1234'
                },
                {
                    key: 'Payment Method',
                    value: 'VISA 5555-****'
                }
            ],
            items: [
                {
                    title: 'Excise',
                    price: '$2.30',
                    quantity: 1
                },
                {
                    title: 'Merchandise Processing Fee',
                    price: '$2.60',
                    quantity: 1
                }
            ],
            tax: '$0.50',
            total: '$5.40',
        });
    }
}


module.exports.RouteDialog = RouteDialog;
