const {MessageFactory, ActivityTypes, InputHints, CardFactory} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const MANAGE_PENDING_WATERFALL_DIALOG = 'managePendingWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')
const AdaptiveCard = require('../resources/adaptiveCard.json');
let User = require('../../models/user.model');

class ManagePendingDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);
        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(MANAGE_PENDING_WATERFALL_DIALOG, [
                this.displayPendingListStep.bind(this),
                this.optionsStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = MANAGE_PENDING_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async displayPendingListStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.MANAGE_PENDING_ORDERS_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        await stepContext.context.sendActivity('Of course, here\'s a list of your orders pending to be shipped.');
        let user = await User.findOne({email: userData.data.email})
        //console.log('user',user)
        await stepContext.context.sendActivity({attachments: [this.createAdaptiveCard()]});
        return await stepContext.next();
    }

    async optionsStep(stepContext) {
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: 'Would you like to ship or track one of these?',
            choices: ChoiceFactory.toChoices(['Ship', 'Track', 'Cancel']),
            //style:ListStyle.heroCard
        })
    }

    async nextStep(stepContext) {
        if (stepContext.result && stepContext.result.index === 0) {
            return await stepContext.replaceDialog(DialogsNameList.SHIPPING_DIALOG);
        } else if (stepContext.result && stepContext.result.index === 1) {
            return await stepContext.replaceDialog(DialogsNameList.TRACKING_DIALOG);
        } else if (stepContext.result && stepContext.result.index === 2) {
            return await stepContext.replaceDialog(DialogsNameList.ANY_HELP_DIALOG);
        }
    }

    createAdaptiveCard() {
        return CardFactory.adaptiveCard(AdaptiveCard);
    }

    // createAdaptiveCard(currierList) {
    //     let items = [];
    //     console.log(currierList);
    //     currierList.forEach(currierItem =>{
    //         let item = [{
    //                 "type": "TextBlock",
    //                 "text":currierItem.postalService ,
    //                 "weight": "bolder",
    //                 "spacing": "medium"
    //             },
    //             {
    //                 "type": "TextBlock",
    //                 "text": currierItem.sendPackageDate,
    //                 "weight": "bolder",
    //                 "spacing": "none"
    //             },
    //             {
    //                 "type": "ColumnSet",
    //                 "separator": true,
    //                 "columns": [
    //                     {
    //                         "type": "Column",
    //                         "width": 1,
    //                         "items": [
    //                             {
    //                                 "type": "TextBlock",
    //                                 "text": currierItem.fromAddress['street-address'] ? currierItem.fromAddress['street-address'] : currierItem.fromAddress.region ? currierItem.fromAddress.region : currierItem.fromAddress.city ? currierItem.fromAddress.city : currierItem.fromAddress.state ? currierItem.fromAddress.state : currierItem.fromAddress.country ,
    //                                 "isSubtle": true
    //                             },
    //                             {
    //                                 "type": "TextBlock",
    //                                 "size": "extraLarge",
    //                                 "color": "accent",
    //                                 "text": currierItem.fromAddress['street-address'] ? (currierItem.fromAddress['admin-area'] ? currierItem.fromAddress['admin-area']: currierItem.fromAddress['country'] ): currierItem.fromAddress.state ? currierItem.fromAddress.state : currierItem.fromAddress.country,
    //                                 "spacing": "none"
    //                             }
    //                         ]
    //                     },
    //                     {
    //                         "type": "Column",
    //                         "width": "auto",
    //                         "items": [
    //                             {
    //                                 "type": "TextBlock",
    //                                 "text": " "
    //                             },
    //                             {
    //                                 "type": "Image",
    //                                 "url": "https://www.pngrepo.com/png/126468/170/service-courier.png",
    //                                 "size": "small",
    //                                 "spacing": "none"
    //                             }
    //                         ]
    //                     },
    //                     {
    //                         "type": "Column",
    //                         "width": 1,
    //                         "items": [
    //                             {
    //                                 "type": "TextBlock",
    //                                 "horizontalAlignment": "right",
    //                                 "text": currierItem.toAddress['street-address'] ? currierItem.toAddress['street-address'] : currierItem.toAddress.region ? currierItem.toAddress.region : currierItem.toAddress.city ? currierItem.toAddress.city : currierItem.toAddress.state ? currierItem.fromAddress.state : currierItem.toAddress.country,
    //                                 "isSubtle": true
    //                             },
    //                             {
    //                                 "type": "TextBlock",
    //                                 "horizontalAlignment": "right",
    //                                 "size": "extraLarge",
    //                                 "color": "accent",
    //                                 "text": currierItem.toAddress['street-address'] ?  (currierItem.toAddress['admin-area'] ? currierItem.toAddress['admin-area']: currierItem.toAddress['country'] ) : currierItem.toAddress.state ? currierItem.toAddress.state : currierItem.toAddress.country,
    //                                 "spacing": "none"
    //                             }
    //                         ]
    //                     }
    //                 ]
    //             }]
    //         items.push(...item);
    //     })
    //
    //     return CardFactory.adaptiveCard({
    //             "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
    //             "version": "1.0",
    //             "type": "AdaptiveCard",
    //             "speak": "Your flight is confirmed for you and 3 other passengers from San Francisco to Amsterdam on Friday, October 10 8:30 AM",
    //             "body": [
    //                 {
    //                     "type": "TextBlock",
    //                     "text": "Pending Currier",
    //                     "weight": "bolder",
    //                     "isSubtle": false
    //                 },
    //                 ...items
    //             ]
    //         }
    //     );
    // }
}

module.exports.ManagePendingDialog = ManagePendingDialog;
