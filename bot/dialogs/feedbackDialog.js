const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const FEEDBACK_WATERFALL_DIALOG = 'feedbackWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const algorithmia = require("algorithmia");

class FeedbackDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);
        this.addDialog(new WaterfallDialog(FEEDBACK_WATERFALL_DIALOG, [
            this.promptStep.bind(this),
            this.nextStep.bind(this)
        ]));
        this.initialDialogId = FEEDBACK_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async promptStep(stepContext) {
        console.log('INSIDE FEEDBACK DIALOG')
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        userData.prevDialog = DialogsNameList.FEEDBACK_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Awesome, before I go, Could you please tell me how was your experience with PB today?'});
    }


    async nextStep(stepContext) {
        let response = await algorithmia.client(process.env.ALGORITHM_KEY).algo("nlp/SentimentAnalysis/1.0.5").pipe({
            "document": stepContext.result
        })
        console.log(response.get()[0].sentiment);
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && response.get()[0].sentiment > 0)
            await stepContext.context.sendActivity('Thanks! It was such a pleasure serving you today, See you soon. Regards Pitney Bowes.')
        else if (stepContext.result && response.get()[0].sentiment < 0) {
            await stepContext.context.sendActivity('We\'re sorry to hear that, we hope we may serve you better next time');
            await stepContext.context.sendActivity('Please contact customer care if problem persists, Regards PB');
        } else {
            await stepContext.context.sendActivity('Thanks for connecting with PB.')
        }
        return await stepContext.endDialog();
    }
}

module.exports.FeedbackDialog = FeedbackDialog;
