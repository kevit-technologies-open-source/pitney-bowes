const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const TRACKING_WATERFALL_DIALOG = 'shippingWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
let userData;

class TrackingDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(TRACKING_WATERFALL_DIALOG, [
                this.getPackageIdStep.bind(this),
                this.tackingChoiceStep.bind(this),
                this.displayInformationStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = TRACKING_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async getPackageIdStep(stepContext) {
        // if(stepContext.options && stepContext.options.retryPrompt)
        //     return await stepContext.next();

        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.TRACKING_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Of course, can you please enter the package id you would like to track?'});
    }

    async tackingChoiceStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        if (stepContext.result && userData.parameters.trackingId) {
            console.log('INSIDE TRACKING ID')
            userData.data.packageId = userData.parameters.trackingId;
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
                prompt: {
                    text: 'Do you want tracking information or cancel ' + userData.data.packageId + ' shipment?'
                    // speak: 'Do you want tracking information or cancel <say-as interpret-as="number_digit">' + userData.data.packageId + '</say-as> shipment?'
                },
                choices: ChoiceFactory.toChoices(['Shipment Details', 'Cancel Shipment']),
                //style: ListStyle.heroCard
            });
        } else {
            console.log('INSIDE REPLACE TRACKING ID')
            return await stepContext.replaceDialog(DialogsNameList.TRACKING_DIALOG, {retryPrompt: true})
        }
    }

    async displayInformationStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        if (stepContext.result && stepContext.result.index === 0) {
            await stepContext.context.sendActivity({
                text: 'Sure, here are the tracking details for your package ' + userData.data.packageId+'.'
                // speak: 'Sure, here are the tracking details for your package  <say-as interpret-as="number_digit">' + userData.data.packageId + '</say-as>.'
            });
            return await stepContext.replaceDialog(DialogsNameList.ANY_HELP_DIALOG);
        } else if (stepContext.result && stepContext.result.index === 1) {
            return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
                prompt: {
                    text: 'Are you sure you want to cancel the package with ' + userData.data.packageId + ' ?'
                    // speak: 'Are you sure you want to cancel the package with <say-as interpret-as="number_digit">' + userData.data.packageId + '</say-as> ?'
                },
                choices: ChoiceFactory.toChoices(['Confirm', 'Cancel']),
                style: ListStyle.heroCard
            });
        }
    }

    async nextStep(stepContext) {
        if (stepContext.result && stepContext.result.index === 0) {
            await stepContext.context.sendActivity('Sure, Your shipment has now been cancelled.');
        }
        return await stepContext.replaceDialog(DialogsNameList.ANY_HELP_DIALOG);
    }
}

module.exports.TrackingDialog = TrackingDialog;
