const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const PRINT_WATERFALL_DIALOG = 'printWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
let User = require('../../models/user.model');
const logger = require('../../services/logger');

class PrintDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);
        this.addDialog(new WaterfallDialog(PRINT_WATERFALL_DIALOG, [
            this.printChoiceStep.bind(this),
            this.nextStep.bind(this)
        ]));
        this.initialDialogId = PRINT_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor;
    }

    async printChoiceStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        userData.prevDialog = DialogsNameList.PRINT_DIALOG
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        await stepContext.context.sendActivity('Perfect! I\'ve scheduled your package pickup with Newgistics on your mentioned time.');
        await stepContext.context.sendActivity('Please make sure your package is ready at the time.');
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: 'Now, would you like me to print this address and packing slip for you?',
            choices: ChoiceFactory.toChoices(['Print', 'Cancel']),
            //style: ListStyle.heroCard
        });
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && stepContext.result.index === 0) {
            userData.data.printAddress = true;
            await stepContext.context.sendActivity('Perfect! your address label and packing slip has been printed');
        } else {
            userData.data.printAddress = false;
        }
        User.findOne({email: userData.data.email}).then(user => {
            user.currier.push(userData.data);
            user.save();
        }).catch(e => {
            logger.log.error('ERROR:', e.message);
        })
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        return await stepContext.replaceDialog(DialogsNameList.ANY_HELP_DIALOG);
    }
}

module.exports.PrintDialog = PrintDialog;
