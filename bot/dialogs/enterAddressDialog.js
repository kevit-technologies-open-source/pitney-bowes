const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const ENTER_ADDRESS_WATERFALL_DIALOG = 'enterAddressWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')

class EnterAddressDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(ENTER_ADDRESS_WATERFALL_DIALOG, [
                this.giveAddressStep.bind(this),
                this.confirmAddressStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = ENTER_ADDRESS_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async giveAddressStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.ENTER_ADDRESS_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Of course, please tell me the address you want to send this package from'})
    }

    async confirmAddressStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        if (stepContext.result && (userData.intent === IntentNameList.GET_ADDRESS_INTENT && userData.parameters.location)) {
            userData.isEnterAddress = true;
            userData.data.toAddress = userData.parameters.location;
            let address = await apiHelper.getGeoLocation(userData.data.toAddress['street-address'] + ' ' + userData.data.toAddress['subadmin-area'])
            console.log('address======>', address);
            userData.data.toAddress.coordinates = address.coordinates;
            userData.data.toAddress.formattedAddress = address.formattedAddress;
            let status = await apiHelper.addressValidation({
                address: address.formattedAddress,
                zipCode: address.postCode
            })
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
                prompt: {
                    text: 'Are you sure you want to send the package to' + address.formattedAddress + '?'
                    // speak: 'Are you sure you want to send the package to <say-as interpret-as="address">' + address.formattedAddress + '</say-as>?'
                },
                choices: ChoiceFactory.toChoices(['Confirm', 'Cancel']),
                //style: ListStyle.heroCard
            });
        } else return await stepContext.replaceDialog(DialogsNameList.PARCEL_DIALOG, {retryPrompt: true})
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && stepContext.result.index === 0) {
            return stepContext.replaceDialog(DialogsNameList.CHECK_ADDRESS_DIALOG);
        } else if (stepContext.result && stepContext.result.index === 1) {
            userData.isToAddress = true;
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            return stepContext.replaceDialog(DialogsNameList.CHANGE_ADDRESS_DIALOG);
        }
    }
}


module.exports.EnterAddressDialog = EnterAddressDialog;
