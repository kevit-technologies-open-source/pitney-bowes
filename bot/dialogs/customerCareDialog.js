const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const CUSTOMER_CARE_WATERFALL_DIALOG = 'messageEmailWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')


class CustomerCareDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);
        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(CUSTOMER_CARE_WATERFALL_DIALOG, [
                this.promptProblemStep.bind(this),
                this.choiceMessageEmailStep.bind(this),
                this.sendMessageEmailStep.bind(this)
            ]));
        this.initialDialogId = CUSTOMER_CARE_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async promptProblemStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        userData.prevDialog = DialogsNameList.CUSTOMER_CARE_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData)
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Could you please tell me what issues are you facing?'});
    }

    async choiceMessageEmailStep(stepContext) {
        let promptMessage = 'Please let me know how would you like us to revert back to you?';
        await stepContext.context.sendActivity('I am really sorry for your inconvenience, PB team will get back to you as soon as possible.');
        return await stepContext.prompt(PromptsNameList.CHOICE_PROMPT, {
            prompt: promptMessage,
            choices: ChoiceFactory.toChoices(['Call', 'Email']),
            //style : ListStyle.heroCard
        })
    }

    async sendMessageEmailStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context)
        if (stepContext.result && stepContext.result.index === 0) {
            return await stepContext.replaceDialog(DialogsNameList.ASK_MOBILE_DIALOG);
        } else if (stepContext.result && stepContext.result.index === 1) {
            if (userData.data.email) {
                await stepContext.context.sendActivity('Thank you so much! We will be getting back to you soon.');
                return await stepContext.replaceDialog(DialogsNameList.ANY_HELP_DIALOG);
            }
            return await stepContext.replaceDialog(DialogsNameList.ASK_EMAIL_DIALOG);
        }
        return await stepContext.replaceDialog(DialogsNameList.ANY_HELP_DIALOG);
    }
}


module.exports.CustomerCareDialog = CustomerCareDialog;
