const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const SCHEDULE_PICKUP_WATERFALL_DIALOG = 'schedulePickUpWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')

class SchedulePickUpDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(SCHEDULE_PICKUP_WATERFALL_DIALOG, [
                this.timePromptStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = SCHEDULE_PICKUP_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async timePromptStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        userData.prevDialog = DialogsNameList.SCHEDULE_PICKUP_DIALOG;
        await this.userInfoPropertyAccessor.set(stepContext.context, userData);
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Of, course, just let me know the time, when you want to schedule the pickup?'});
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && userData.parameters.date) {
            userData.data.sendPackageDate = userData.parameters.date
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            if (!userData.parameters.time)
                return await stepContext.beginDialog(DialogsNameList.TIME_SCHEDULE_PICKUP_DIALOG);
        } else if (stepContext.result && userData.parameters.time) {
            userData.data.sendPackageTime = userData.parameters.time
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            if (!userData.parameters.date)
                return await stepContext.beginDialog(DialogsNameList.DATE_SCHEDULE_PICKUP_DIALOG);
        } else {
            return await stepContext.replaceDialog(DialogsNameList.SCHEDULE_PICKUP_DIALOG);
        }
        return await stepContext.replaceDialog(DialogsNameList.PRINT_DIALOG);
    }
}


module.exports.SchedulePickUpDialog = SchedulePickUpDialog;
