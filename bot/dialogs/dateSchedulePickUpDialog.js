const {MessageFactory, ActivityTypes, InputHints} = require('botbuilder');
const {TextPrompt, ListStyle, ChoicePrompt, ChoiceFactory, WaterfallDialog} = require('botbuilder-dialogs');
const DialogsNameList = require('./../../shared/DialogsNameList');
const IntentNameList = require('./../../shared/IntentNameList');
const DATE_SCHEDULE_PICKUP_WATERFALL_DIALOG = 'dateSchedulePickUpWaterfallDialog';
const PromptsNameList = require('../../shared/PromptsNameList');
const {DialogFlowRecognizer} = require('../dialogs/dialogFlowRecognizer');
const apiHelper = require('../../helper/api.helper')

class DateSchedulePickUpDialog extends DialogFlowRecognizer {
    constructor(dialogId, userInfoPropertyAccessor) {
        super(dialogId);

        this.addDialog(new TextPrompt(PromptsNameList.TEXT_PROMPT))
            .addDialog(new WaterfallDialog(DATE_SCHEDULE_PICKUP_WATERFALL_DIALOG, [
                this.timePromptStep.bind(this),
                this.nextStep.bind(this)
            ]));
        this.initialDialogId = DATE_SCHEDULE_PICKUP_WATERFALL_DIALOG;
        this.userInfoPropertyAccessor = userInfoPropertyAccessor
    }

    async timePromptStep(stepContext) {
        return await stepContext.prompt(PromptsNameList.TEXT_PROMPT, {prompt: 'Cool, and which day?'});
    }

    async nextStep(stepContext) {
        let userData = await this.userInfoPropertyAccessor.get(stepContext.context);
        if (stepContext.result && userData.parameters.date) {
            userData.data.sendPackageDate = userData.parameters.date;
            await this.userInfoPropertyAccessor.set(stepContext.context, userData);
            return await stepContext.replaceDialog(DialogsNameList.PRINT_DIALOG);
        } else return await stepContext.replaceDialog(DialogsNameList.DATE_SCHEDULE_PICKUP_DIALOG);
    }
}


module.exports.DateSchedulePickUpDialog = DateSchedulePickUpDialog;
