module.exports = {

    FLOW_NEXT_QUESTION_ACCESSOR: 'flowNextQuestionAccessor',
    FLOW_BUILDER_CONVERSATION_ACCESSOR: 'flowBuilderConversationAccessor',
    /*
          ->This Properties is for Greeting Dialog
       */
    SURVEY_SEQUENCE_LIST: 'surveySequenceList',
    IS_SECOND_TIME_OPENING_SENTENCE_ACCESSOR: 'isSecondTimeOpeningSentenceAccessor',

    /*
          ->This Properties is For Survey Handler Dialog
       */
    SURVEY_NEXT_QUESTION: 'surveyNextQuestion',
    SEQUENCE_SURVEY_HANDLER_LIST: 'sequenceSurveyHandlerList',
    PREVIOUS_SURVEY_QUESTION_LIST: 'previousSurveyQuestionList',
    CURRENT_QUESTION: 'currentQuestion',
    SURVEY_RESPONSE_ACCESSOR: 'surveyResponseAccessor',
    CONVERSATION_ACCESSOR: 'conversationAccessor',
    CATEGORY_MAP_ACCESSOR: 'categoryMapAccessor',
    RESULT_SURVEY_ACCESSOR: 'resultSurveyAccessor',
    CURRENT_RESULT_CATEGORY: 'currentResultCategory',
    GUIDIANCE_RESULT_CONDITION_CATEGORY_MAP: "guidianceResultConditionCategoryMap",
    /*
          -> This Properties is for Quit Dialog
      */
    CURRENT_DIALOG_STATUS_ACCESSOR: 'currentDialogStatusAccessor',
    QUIT_DIALOG_STATUS_ACCESSOR: 'quitDialogStatusAccessor',

    /*
          -> This Properties is for Qna Dialog and Qna Child Dialog
      */
    CURRENT_USER_QNA_QUESTION_ACCESSOR: 'currentUserQnaQuestionAccessor',
    NO_U_KNOWN_TRY_ACCESSOR: 'noUknownTryAccessor',
    QNA_RESPONSE_OBJECT_ACCESSOR: 'qnaResponseObjectAccessor',
    QNA_OPENING_COUNT_ACCESSOR: 'qnaOpeningCountAccessor',
    QNA_CONFIG_COUNT_ACCESSOR: 'qnaConfigCountAccessor',

    /*
      --> Help The Bot Properties
      */
    TOTAL_TAGS_ACCESSOR: 'totalTagsAccessor',
    START_TAG_INFO_ACCESSOR: 'startTagInfoAccessor',
    END_TAG_INFO_ACCESSOR: 'endTagInfoAccessor',
    UNRECOGNIZED_QUESTIONS_ACCESSOR: 'unRecognizedQuestionsAccessor',
    UNRECOGNIZED_CURRENT_QUESTION_ACCESSOR: 'unRecognizedCurrentQuestionAccessor',

    /*
       --> Qna Notify Properties
     */
    UNRECOGNIZED_NOTIFY_QUESTIONS_ACCESSOR: 'unRecognizedNotifyQuestionsAccessor',
    UNRECOGNIZED_CURRENT_NOTIFY_QUESTION_ACCESSOR: 'unRecognizedCurrentNotifyQuestionAccessor',

    /*
       -->  Ask Question From Survey Properties
     */
    ASKED_QUESTION_FROM_SURVEY_ACCESSOR: 'askedQuestionFormSurveyAccessor',

    /*
      --> HandOff Handler Properties
       */
    IS_HAND_OFF_ACCESSOR: 'isHandOffAccessor',
    CURRENT_HAND_OFF_ACCESSOR: 'currentHandOffAccessor',
    PREVIOUS_HAND_OFF_ACCESSOR: 'previousHandOffAccessor',

    /*
      --> Unsubscribe Handler Properties
     */
    SUBSCRIPTION_LIST_ACCESSOR: 'subscriptionListAccessor',
    SUBSCRIPTION_START_POINT_ACCESSOR: 'subscriptionStartPointAccessor',
    SUBSCRIPTION_END_POINT_ACCESSOR: 'subscriptionEndPointAccessor',
    ACTIVE_SUBSCRIPTION_LIST_ACCESSOR: 'activeSubscriptionListAccessor',
    /*
     * --> Conversation Block State Property
     */
    IS_CONVERSATION_BLOCK_ACCESSOR: 'isConversationBlockAccessor'

};
