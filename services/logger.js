/**
 * Services Structure
 */

/**
 * System and 3rd party libs
 */
const bunyan = require('bunyan');
const morgan = require('morgan');

/**
 * Declarations & Implementations
 */
let log = bunyan.createLogger({
    name: 'Bot-Framework',
    version: process.env.VERSION,
});
log.level(process.env.LOG_LEVEL);

let morganInstance = morgan(
    ':remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms' +
    ' "RequestId: :id"',
    {
        stream: {
            write: str => {
                log.info(str);
            },
        },
    },
);
// Get generated ID as token for requests.
morgan.token('id', req => {
    return req.id;
});
/**
 * Service Export
 */
module.exports = {
    log: log,
    morgan: morganInstance,
};
