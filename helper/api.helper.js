const rp = require('request-promise');
const LocationToken = require('../models/location-token.model');
const logger = require('../services/logger');
const url=require('url');

let getAccessToken = () => {
    let mergedKey=process.env.PB_API_KEY+':'+process.env.PB_API_SECRET;
    let buff = new Buffer(mergedKey);
    let base64dataAuthorization = buff.toString('base64');
    return LocationToken.find({}).then(locationToken => {
        if (locationToken.length > 0) {
            logger.log.info('Token from DB ->', locationToken[0].accessToken);
            return locationToken[0].accessToken;
        } else {
            let options = {
                method: 'POST',
                url: 'https://api.pitneybowes.com/oauth/token',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Basic ' + base64dataAuthorization
                },
                body: 'grant_type=client_credentials'
            };

            return rp(options).then(body => {
                let accessToken = JSON.parse(body).access_token;
                logger.log.info('Get new Token ->', accessToken);
                return new LocationToken({accessToken}).save(l => {
                    logger.log.info('New Token Updated')
                    return accessToken
                });
            }).catch(e => {
                logger.log.error('ERROR :', e.message);
                return null;
            })
        }
    }).catch(e => {
        logger.log.error('ERROR : ', e.message)
        return null
    })
}

let getGeoLocationByIPAddress = async (ip) => {
    let accessToken = await getAccessToken();
    logger.log.info('accessToken', accessToken);
    let options = {
        method: 'GET',
        url: 'https://api.pitneybowes.com/location-intelligence/geolocation/v1/location/byipaddress',
        qs: {ipAddress: ip},
        headers: {
            Authorization: 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    };

    return rp(options).then(body => {
        logger.log.info('body', JSON.parse(body));
        let response = JSON.parse(body)
        return {
            continent: response.ipInfo.place.continent,
            region: response.ipInfo.place.region,
            city: response.ipInfo.place.city.value,
            country: response.ipInfo.place.country.value,
            state: response.ipInfo.place.state.value,
            postCode: response.ipInfo.place.postCode,
        }
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })
}

let geoSearch = async (searchObj) => {
    let accessToken = await getAccessToken();
    logger.log.info('accessToken', accessToken);
    let options = {
        method: 'GET',
        url: 'https://api.pitneybowes.com/location-intelligence/geosearch/v2/locations',
        qs: {
            searchText: 'times%20sq',
            latitude: '40.761819',
            longitude: '-73.997533',
            searchRadius: '52800',
            searchRadiusUnit: 'Feet',
            maxCandidates: '10',
            country: '{{country}}',
            matchOnAddressNumber: '{{matchOnAddressNumber}}',
            autoDetectLocation: 'true',
            ipAddress: '{{ipAddress}}',
            areaName1: '{{areaName1}}',
            areaName3: '{{areaName3}}',
            postCode: '{{postCode}}',
            returnAdminAreasOnly: 'N',
            includeRangesDetails: 'Y'
        },
        headers: {
            Authorization: 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    };

    return rp(options).then(body => {
        logger.log.info('body', JSON.parse(body));
        let response = JSON.parse(body)
        return {}
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })
}

let getIdentity = async (address) => {
    let accessToken = await getAccessToken();
    let addressStr = address.region ? address.region + ', ' + address.city + ', ' + address.state + ', ' + address.country + ' ' + address.postCode :
        address['business-name'] + ', ' + address['street-address'] + ', ' + address['subadmin-area'] + ', ' + address['admin-area'] + ', ' + address['city'] + ', ' + address['country'] + ', ' + address['zip-code'];
    logger.log.info('accessToken', accessToken);
    let options = {
        method: 'GET',
        url: addressStr,
        qs: {
            address: '4750%20Walnut%20St,%20Boulder,%20CO%2080301',
            confidence: 'low',
            givenName: '',
            familyName: '',
            maxCandidates: '',
            theme: '',
            filter: ''
        },
        headers: {
            Authorization: 'Bearer blSmEfvbR8Xyv9KKBQ92HfRstzpv',
            Accept: 'application/json'
        }
    };

    return rp(options).then(body => {
        logger.log.info('body', JSON.parse(body));
        let response = JSON.parse(body)
        return {}
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })
}

let getRouteByAddress = async (addressObj) => {
    let accessToken = await getAccessToken();
    logger.log.info('accessToken', accessToken);
    let options = {
        method: 'GET',
        url: 'https://api.pitneybowes.com/location-intelligence/georoute/v1/route/byaddress',
        qs: {
            startAddress: '4750%20Walnut%20St,%20Boulder,%20CO',
            endAddress: '2935%20Broadbridge%20Ave,%20Stratford,%20CT',
            db: 'driving',
            country: 'USA',
            intermediateAddresses: '',
            oip: 'false',
            destinationSrs: 'epsg:4326',
            optimizeBy: 'time',
            returnDistance: 'true',
            distanceUnit: 'm',
            returnTime: 'true',
            timeUnit: 'min',
            language: 'en',
            directionsStyle: 'None',
            segmentGeometryStyle: 'none',
            primaryNameOnly: 'false',
            majorRoads: 'false',
            historicTrafficTimeBucket: 'None',
            returnDirectionGeometry: 'false'
        },
        headers: {
            Authorization: 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    };

    return rp(options).then(body => {
        logger.log.info('body', JSON.parse(body));
        let response = JSON.parse(body)
        return {}
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })

}

let getAddressByLanAndLag = async (lan, lag) => {
    let accessToken = await getAccessToken();
    let options = {
        method: 'GET',
        url: 'https://api.pitneybowes.com/location-intelligence/geocode-service/v1/transient/premium/reverseGeocode',
        qs: {
            x: lan,
            y: lag,
            country: 'USA'
        },
        headers: {
            Authorization: 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    };
    return rp(options).then(body => {
        console.log('body', JSON.stringify(JSON.parse(body), null, 2));
        let response = JSON.parse(body)
        return response;
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })
}

let getAddressByGoogleLanAndLag = async (lan, lag) => {
    let options = {
        method: 'POST',
        url: 'https://maps.googleapis.com/maps/api/geocode/json',
        qs:
            {
                latlng: lan + ',' + lag,
                key: process.env.GOOGLE_MAP_KEY
            },
        headers:
            {
                'Postman-Token': '7762e70c-d0ad-4417-a6d7-4ac1ee8ef137',
                'cache-control': 'no-cache'
            }
    };

    return rp(options).then(body => {
        return body;
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })
}

let getGeoLocation = async (address) => {
    let accessToken = await getAccessToken();
    let options = {
        method: 'GET',
        url: 'https://api.pitneybowes.com/location-intelligence/geosearch/v2/locations',
        qs: {
            searchText: address,
            maxCandidates: '1',
            country: 'USA'
        },
        headers: {
            Authorization: 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    };
    // console.log(JSON.stringify(options,null,2));
    return rp(options).then(body => {
        console.log('body', JSON.stringify(JSON.parse(body), null, 2));
        let response = JSON.parse(body)
        return {...response.location[0].address, coordinates: response.location[0].geometry.coordinates}
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })
}

let addressValidation = async (address) => {
    let accessToken = await getAccessToken();
    let options = {
        method: 'POST',
        url: 'https://api-sandbox.pitneybowes.com/shippingservices/v1/addresses/verify',
        qs: {minimalAddressValidation: 'false'},
        headers: {
            'X-PB-UnifiedErrorStructure': 'true',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + accessToken
        },
        body: {
            addressLines: [address.address],
            postalCode: address.zipCode,
            countryCode: 'US'
        },
        json: true
    };

    return rp(options).then(body => {
        console.log('body', JSON.stringify(JSON.parse(body), null, 2));
        let response = JSON.parse(body)
        return response.error ? response.error : response.status
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })
}

let geoRoute = async (from, to) => {
    let accessToken = await getAccessToken();
    let options = {
        method: 'GET',
        url: 'https://api.pitneybowes.com/location-intelligence/georoute/v1/route/byaddress',
        qs: {
            startAddress: from,
            endAddress: to,
            country: 'USA'
        },
        headers: {
            Authorization: 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    };
    return rp(options).then(body => {
        console.log('body', JSON.stringify(JSON.parse(body), null, 2));
        return JSON.parse(body);
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })
}

let riskEarthquake = async (lan, long) => {
    let accessToken = await getAccessToken();
    let options = {
        method: 'GET',
        url: 'https://api.pitneybowes.com/location-intelligence/georisk/v1/earthquake/bylocation',
        qs: {longitude: long, latitude: lan},
        headers: {
            Authorization: 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    };
    return rp(options).then(body => {
        console.log('body', JSON.stringify(JSON.parse(body), null, 2));
        let response = JSON.parse(body)
        return response.riskLevel;
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })
}

let riskFlood = async (lan, long) => {
    let accessToken = await getAccessToken();
    let options = {
        method: 'GET',
        url: 'https://api.pitneybowes.com/location-intelligence/georisk/v1/flood/bylocation',
        qs: {longitude: long, latitude: lan},
        headers:
            {
                Authorization: 'Bearer ' + accessToken,
                'Content-Type': 'application/json',
                Accept: 'application/json'
            }
    };

    return rp(options).then(body => {
        console.log('body', JSON.stringify(JSON.parse(body), null, 2));
        let response = JSON.parse(body)
        return response.floodZone.riskLevel;
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })
}

let riskFire = async (lan, long) => {
    let accessToken = await getAccessToken();
    let options = {
        method: 'GET',
        url: 'https://api.pitneybowes.com/location-intelligence/georisk/v1/fire/bylocation',
        qs: {longitude: long, latitude: lan},
        headers: {
            Authorization: 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    };

    return rp(options).then(body => {
        console.log('body', JSON.stringify(JSON.parse(body), null, 2));
        let response = JSON.parse(body)
        return response.fireShed.risk.description;
    }).catch(e => {
        logger.log.error('ERROR :', e.message)
        return null;
    })

}

module.exports = {
    getGeoLocationByIPAddress,
    geoSearch,
    getIdentity,
    getRouteByAddress,
    getAddressByLanAndLag,
    getAddressByGoogleLanAndLag,
    getGeoLocation,
    addressValidation,
    geoRoute,
    riskEarthquake,
    riskFlood,
    riskFire
};
