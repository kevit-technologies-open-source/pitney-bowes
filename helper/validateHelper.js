const validateAndResult = (userInput, validArray) => {
    let rg = new RegExp(RegExp.escape(userInput), 'gi');
    return validArray.find(element => {
        if (rg.test(element)) {
            return true;
        }
    });
};
RegExp.escape = function (userInput) {
    return userInput.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};
module.exports = {
    validateAndResult,
};
