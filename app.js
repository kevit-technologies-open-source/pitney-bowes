/**
 * System and 3rd party libs
 */
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fs = require('fs');
const mongoose = require('mongoose');
const cors = require('cors');
/**
 * Required Services
 */
const Logger = require('./services/logger');
/**
 * Global declarations
 */
const models = path.join(__dirname, 'models');
const dbURL = process.env.MONGODB_URL || 'mongodb://db:27017/Hackathon';

/**
 * Generating index.html from template
 */
let indexTemplate = fs.readFileSync(path.join(__dirname, 'template', 'index.html')).toString();
let replacingEntities = ['GOOGLE_MAPS_API_KEY', 'DIRECTLINE_SECRET'];
replacingEntities.forEach(replacingString => {
    indexTemplate=indexTemplate.replace('{{' + replacingString + '}}', process.env[replacingString]);
});
fs.writeFileSync(path.join(__dirname, 'front', 'index.html'), indexTemplate);

/**
 * Bootstrap Models
 */
//fs.readdirSync(models).forEach(file => require(path.join(models, file)));


/**
 * Bootstrap App
 */
const app = express();
app.use(Logger.morgan);
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: false,
    }),
);
app.use(cookieParser());

// Allow CORS
app.use(
    cors({
        origin: true,
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
        allowedHeaders: ['Origin', ' X-Requested-With', ' Content-Type', ' Accept ', ' Authorization'],
        credentials: true,
    }),
);

/**
 * Import and Register Routes
 */
const index = require('./routes/index');
const bot = require('./bot/index');

app.use('/bot', bot);
app.use('/', express.static(path.join(__dirname, 'front'), {index: 'index.html'}));
app.use('/front', express.static(path.join(__dirname, 'front')));

/**
 * Mongoose Configuration
 */
mongoose.Promise = global.Promise;

mongoose.connection.on('connected', () => {
    Logger.log.info('DATABASE - Connected');
});

mongoose.connection.on('error', (err) => {
    Logger.log.error('DATABASE - Error:' + err);
});

mongoose.connection.on('disconnected', () => {
    Logger.log.warn('DATABASE - disconnected  Retrying....');
});

let connectDb = function () {
    const dbOptions = {
        poolSize: 5,
        reconnectTries: Number.MAX_SAFE_INTEGER,
        reconnectInterval: 500,
        useNewUrlParser: true
    };
    mongoose.connect(dbURL, dbOptions)
        .catch(err => {
            Logger.log.fatal('DATABASE - Error:' + err);
        });
};

connectDb();


module.exports = {app};
